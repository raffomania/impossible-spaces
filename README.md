# Overlapping Redirectors

The Overlapping Redirector implements a redirection technique commonly known as 'Impossible Spaces'. It works by allowing rooms in the virtual world to overlap each other, making them larger than they could possibly be in reality. To make this unnoticeable to the user, rooms that are out of their viewing range are disabled, resulting in only one of multiple overlapping rooms being visible at the same time. For more details, see the original paper by Suma et al [0].

The Space Extender toolkit allows you to add overlapping spaces to your game with the following features:
    - Interactable objects get hidden along with overlapping rooms, no matter if they're children of the room or not
    - Works with additional scene loading (enabling per-room GI baking, among others)
    - Provides an automatically generated 3D minimap with a player position indicator
    - Allows smooth transitions when hiding rooms, for e.g. animating doors or smoothly dimming lights

## Getting Started

First of all, add an `OverlappingRedirectorPlayer` Component to the RigidBody and Collider attached to your player. The RigidBody can be Kinematic or dynamic, but not static. The Collider should not be a trigger.
This allows the Overlapping Redirectors to know where the player is and when to show which overlapping rooms.

Now, you configure overlapping spaces using the `OverlappingRedirector`. For each room that overlaps another room, create a redirector object using the `OverlappingRedirector` prefab. The structure of this object looks like this:

Redirector
  TransitionArea
  InteriorArea

Drag one of these into your scene. To configure it:

1. Find the object you want to hide when the player is not in this room. If you want to hide multiple objects, make them children of a common parent object. Usually, you can simply hide the whole room. Set this object as the "Object to Hide" Reference on the Redirector Component.
2. Transform the `InteriorArea` collider so it covers the area inside the room. When the player is inside this area, the Redirector will hide other rooms. It should cover every part of the room's interior, especially areas overlapping with other rooms, but nothing outside of it.
3. Transform the `TransitionArea` collider so it covers the area outside the room in which the entry to the room is still visible. When the player enters this collider, the room will be made visible, and when he leaves it without entering the room, it will be hidden again.
Make sure to let the transition and interior area overlap a little. Otherwise, the room will get hidden when the player leaves the transition area and hasn't entered the interior area yet. You can let the transition area cover a bit of the room's interior. Do not, however, let the interior area cover parts of the transition area outside of the room.

## Adding a Minimap

To add a minimap, add the `Minimap` Component to any GameObject. You can add it as a child on any object in your scene, or on one of the player's hands. 
You can make it hide itself when the player is not looking at it, and rotate itself to keep a certain orientation in relation to the player's head.

Set up the references:

- Material: The material the meshes in the minimap should have.
- Head: The transform indicating the position and rotation of the player's head.
- Player Pin: The minimap component will set this transform to the position the player has relative to the minimap. Create a game object you want to indicate your player's position on the minimap and drag it into this slot.
- North Is Always Up: Whether the map should rotate so that the same side always points upward. The alternative (if you uncheck this) is to align the map with the player's view, meaning the map's global rotation will always equal the level's global rotation.
- Hand Transform: Optional. Position of the player's hand the minimap is attached to. If you provide this, the minimap will rotate itself on the back of the hand to provide a "wrist watch" look.
- Only Show When Player Looks At Hand: Whether to hide the minimap when the player doesn't look at his hand. If you activate this, you also have to set the Hand Transform.

When all references are set up, click the "regenerate minimap" button in the minimap component's inspector view. It will look at *all* game objects in all scenes and clone the ones with all of the following properties:
    - contains a `Collider` that is not a trigger
    - contains a `MeshFilter`
    - is static

After the minimap has been generated, you will probably want to make the minimap smaller. By default, it is generated at the same size as the level. To resize it, change the scale of the object you attached the `Minimap` script to.

You can then transform the rooms you have added to redirectors in the generated minimap so none of them overlap. When regenerating the map the next time, the minimap will remember the transformations you applied to the rooms and store them in a `MinimapRoom` component on the room objects in the "real" world to remember them for the next map generation.

After transforming the rooms in the minimap, be sure to hit "regenerate minimap" once again, as this will generate information used at runtime to correctly map the player's position to their position on the minimap.

## Using Transitions

Use transitions to make a room wait for an event before it hides or shows itself.
For example, you can animate a door and only hide the room after it has been closed, so the player
doesn't notice it's disappearance.

To use transitions, create a new GameObject and add the `OverlappingRoomTransition` Component.
Then add your own script to control the transition, like this:

```csharp
    public void Start()
    {
        transition = GetComponent<OverlappingRoomTransition>();
        transition.RaiseTransitionStart += OnTransitionStart;
    }

    public void OnTransitionStart(object sender, bool active) 
    {
        // start the transition, e.g. by triggering an animation
    }

    // This might be called by e.g. an animator when the animation ends
    public void OnTransitionEnd()
    {
        // the redirector waits until you call this before hiding or showing itself
        transition.EndTransition();
    }
```


## Tips and Tricks

### Covering complex room layouts
The default prefab contains one box collider for each area, but you can easily replace this with other collider types or add additional children with more colliders to cover complex room layouts, making it a compound collider. If you use nested colliders, you might need to add a kinematic RigidBody to your topmost collider, as the collision events might not get sent for child colliders without it.

### Dealing with baked GI
When baking GI for overlapping rooms, you might run into problems. Since Unity has a single GI lightmap per scene, when baking with multiple rooms being visible, you'll see artifacts in the overlapping area when hiding one of them.
The recommended solution to this is creating a scene for each overlapping room and loading them using additive scene loading. This way, you can bake a separate lightmap for each room. Take care to move the corresponding Redirectors into the scenes where their rooms are, since Unity can't reference objects across scenes and each Redirector needs a reference to the room it's assigned to.
Be careful with lights in overlapping areas. They might cause sudden changes in (realtime) lighting when rooms are activated or deactivated.
To bake each scene individually, you will have to double-click each scene to mark it as the current main scene, and then unload other scenes that overlap with the one you are baking.
Only marking other rooms as inactive will clear their lightmap information when you bake.
The Minimap component will scan all scenes that are loaded when you regenerate the map and automatically find all redirector objects.

## Handling dynamic rigidbodies
Redirectors will track dynamic rigidbody objects that enter their interior area collider, and when the room gets disabled, the rigidbodies it contains will get disabled as well.
When the user is in multiple transition areas at the same time, this usually means that he can see entries to multiple overlapping rooms. As a consequence, the Redirectors will make both rooms active, which may cause some rigidbodies in the overlapping area to move. Currently, there is no solution to this other than to make the transition areas not overlap.


## Limitations

There is no built-in multiplayer support. If there are multiple `OverlappingRedirectorPlayer`s in a scene, things will most likely break.

## References

[0] E. A. Suma, Z. Lipps, S. Finkelstein, D. M. Krum and M. Bolas, "Impossible Spaces: Maximizing Natural Walking in Virtual Environments with Self-Overlapping Architecture," in IEEE Transactions on Visualization and Computer Graphics, vol. 18, no. 4, pp. 555-564, April 2012, doi: 10.1109/TVCG.2012.47.