﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(Rigidbody))]
public class TriggerEvents : MonoBehaviour
{
    public UnityEvent OnTriggerEnterEvent;
    public UnityEvent OnTriggerExitEvent;
    public UnityEvent OnTriggerStayEvent;
    public string triggerTag = "Player";

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == triggerTag)
        {
            OnTriggerEnterEvent.Invoke();
        }
    }

    void OnTriggerStay(Collider other)
    {
        if(other.tag == triggerTag)
        {
            OnTriggerStayEvent.Invoke();
        }
    }

    void OnTriggerExit(Collider other)
    {
        if(other.tag == triggerTag)
        {
            OnTriggerExitEvent.Invoke();
        }
    }
}
