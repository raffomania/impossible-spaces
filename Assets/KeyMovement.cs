﻿using UnityEngine;
using System.Collections;

// This script moves the character controller forward
// and sideways based on the arrow keys.
// It also jumps when pressing space.
// Make sure to attach a character controller to the same game object.
// It is recommended that you make only one call to Move or SimpleMove per frame.

public class KeyMovement : MonoBehaviour
{
    CharacterController characterController;

    public float speed = 6.0f;
    public float jumpSpeed = 8.0f;
    public float gravity = 20.0f;

    private float yaw = 0.0f;
    private float pitch = 0.0f;
    private Vector3 moveDirection = Vector3.zero;

    void Start()
    {
        characterController = GetComponent<CharacterController>();
    }

    void Update()
    {
        if (Input.GetAxis("Fire2") != 0)
        {

            yaw += 50 * Time.deltaTime * Input.GetAxis("Mouse X");
            pitch -= 50 * Time.deltaTime * Input.GetAxis("Mouse Y");

            transform.eulerAngles = new Vector3(pitch, yaw, 0f);
        }

        if (characterController.isGrounded)
        {
            // We are grounded, so recalculate
            // move direction directly from axes

            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
            moveDirection *= speed;
            moveDirection = Quaternion.AngleAxis(transform.rotation.eulerAngles.y, Vector3.up) * moveDirection;

            if (Input.GetButtonUp("Jump"))
            {
                var grabbable = GameObject.Find("Grabbable");
                if (grabbable.transform.parent == transform) {
                    grabbable.transform.SetParent(transform.parent);
                    grabbable.GetComponent<Rigidbody>().isKinematic = false;
                } else {
                    grabbable.transform.SetParent(transform);
                    grabbable.GetComponent<Rigidbody>().isKinematic = true;
                }
                // moveDirection.y = jumpSpeed;
            }
        }

        // Apply gravity. Gravity is multiplied by deltaTime twice (once here, and once below
        // when the moveDirection is multiplied by deltaTime). This is because gravity should be applied
        // as an acceleration (ms^-2)
        moveDirection.y -= gravity * Time.deltaTime;

        // Move the controller
        characterController.Move(moveDirection * Time.deltaTime);
    }
}
